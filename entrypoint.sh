#!/bin/sh

set -e
envsubst < /etc/nginx/default.conf.tpl > etc/nginx/cong.d/default.conf
nginx -g 'daemon off;'